$(function() {
    $('#picture').change(function() {
        
	$("#custom_preview").show();
	$('#custom_preview').html('<img src="/assets/images/ajaxloader.gif" width="128" height="128" >');
	
	var file_data = $("#picture").prop("files")[0];   // Getting the properties of file from file field
	var form_data = new FormData();                  // Creating object of FormData class
	form_data.append("ImageFile", file_data)              // Appending parameter named file with properties of file_field to form_data
	
	   
	   $.ajax({
			url: "/upload_picture",
			dataType: 'script',
			cache: false,
            contentType: false,
			processData: false,
			data : form_data,
			type: "POST",
			success: function(data)
			{
				data = data.replace('"', '');
				$("#custom_preview").show();
				$("#custom_preview").html('<img src="/assets/uploads/gallery/'+data+'" />');
				jcrop_call();
			},
			error: function ()
			{
			}
		});
	   
	   
	 

    });
});