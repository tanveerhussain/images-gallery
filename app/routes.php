<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::post('/', 'HomeController@searchByTitle');
Route::get('/add-new-image', 'HomeController@add_new_image_form');
Route::post('/add-new-image', 'HomeController@save_image');
Route::get('/tags/{any}', 'HomeController@searchByTag');
Route::get('/tags/', 'HomeController@index');
Route::get('/download/{any}', 'HomeController@force_download');
Route::get('/download/', 'HomeController@index');
Route::post('/upload_picture', 'HomeController@upload_picture');