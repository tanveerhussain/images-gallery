<?php
class Tag extends Eloquent  {
	public $timestamps = false;
	public function image()
    {
        return $this->belongsTo('Image');
    }
}
