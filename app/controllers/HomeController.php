<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		$images = Image::has('tags')->get();
		return View::make('index')->with('images', $images)->with('keyword', '');
	}
	
	public function searchByTitle()
	{
		$images = Image::has('tags')->where('title', 'LIKE', '%'.Input::get('keyword').'%')->get();
		return View::make('index')->with('images', $images)->with('keyword', Input::get('keyword'));
	}
	
	public function searchByTag()
	{ 
		$images = Image::whereHas('tags', function($q)
				{
					$q->where('tag', '=', urldecode(Request::segment(2)));
				
				})->get();
		
		return View::make('index')->with('images', $images)->with('keyword', '');
	}
	
	public function force_download()
	{ 
		$image = Image::find(Request::segment(2));
		$image_path = $image->path;
		$image_path = public_path()."/assets/uploads/gallery/".$image_path;
		$image_path = str_replace("\\", "/", $image_path);
		return Response::download($image_path);
	}
	
	
	
	
	public function add_new_image_form()
	{
		return View::make('image_form');
	}
	
	public function save_image(){
		
		$rules = array(
			'title'    => 'required',
			'description'    => 'required',
			'tags'    => 'required',
			'picture'    => 'required_if:path,|image'
		 );
		 $validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('/add-new-image')->withErrors($validator)->withInput(); 
		}else{
			$fileName = '';
			if (Input::has('path')){
					
					$targ_w = $targ_h = 110;
					$jpeg_quality = 90;
					
					$fileName = Input::get('path');
					$src = public_path().'\assets\uploads\gallery\temp\thumb_'.$fileName;
					
					
					$ImageType = Input::get('image_type');
					switch(strtolower($ImageType))
					{
						case 'image/png':
							//Create a new image from file 
							$img_r =  imagecreatefrompng($src);
							break;
						case 'image/gif':
							$img_r =  imagecreatefromgif($src);
							break;			
						case 'image/jpeg':
						case 'image/pjpeg':
							$img_r = imagecreatefromjpeg($src);
							break;
						default:
							die('Unsupported File!'); //output error and exit
					}
					
					$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
					
					$DestFolder = public_path().'\assets\uploads\gallery\thumb_'.$fileName;
					
					if (Input::has('x') and Input::has('y')){
					
						if(imagecopyresampled($dst_r,$img_r,0,0,Input::get('x'),Input::get('y'),$targ_w,$targ_h,Input::get('w'),Input::get('h')))
						{
							//header("Content-type: $ImageType");
							switch(strtolower($ImageType))
							{
								case 'image/png':
									imagepng($dst_r,$DestFolder);
									break;
								case 'image/gif':
									imagegif($dst_r,$DestFolder);
									break;			
								case 'image/jpeg':
								case 'image/pjpeg':
									imagejpeg($dst_r,$DestFolder,$jpeg_quality);
									break;
								default:
									return false;
							}
						//Destroy image, frees memory	
						if(is_resource($dst_r)) {imagedestroy($dst_r);} 
							//return true;
						 }
					 
					}else{
					
						list($CurWidth,$CurHeight)=getimagesize($src);
						$ThumbSquareSize = $targ_w;
						$this->cropImage($CurWidth, $CurHeight, $ThumbSquareSize, $DestFolder,$img_r, $jpeg_quality, $ImageType);	
						
					}
					 
					$src = str_replace("\\", "/", $src); 
					File::delete($src);
					
				}
			
			$image = new Image;
			$image->title = Input::get('title');
			$image->description = Input::get('description');
			$image->path = $fileName;
			$image->save();	
			
			$tags_bulk_data = array();
			$tags = explode(",", Input::get('tags'));
			if(count($tags) > 0){
				foreach($tags as $index=>$t){
					$tags_bulk_data[$index]['image_id'] = $image->id;
					$tags_bulk_data[$index]['tag'] = trim($t);
				}
			}
			
			if(!empty($tags_bulk_data)){
				Tag::insert($tags_bulk_data);
			}
			
			return Redirect::to('/add-new-image')->with('flash_msg', 'You have successfully added new image.');	
		}
		
	}
	
	public function upload_picture(){
			
			if (!file_exists('assets/uploads')) {
    				mkdir('assets/uploads', 0777, true);
			}
			
			if (!file_exists('assets/uploads/gallery')) {
    				mkdir('assets/uploads/gallery', 0777, true);
			}
			
			if (!file_exists('assets/uploads/gallery/temp')) {
    				mkdir('assets/uploads/gallery/temp', 0777, true);
			}
			
			$ImageType = $_FILES['ImageFile']['type'];
			switch(strtolower($ImageType))
			{
				case 'image/png':
					//Create a new image from file 
					$CreatedImage =  imagecreatefrompng($_FILES['ImageFile']['tmp_name']);
					break;
				case 'image/gif':
					$CreatedImage =  imagecreatefromgif($_FILES['ImageFile']['tmp_name']);
					break;			
				case 'image/jpeg':
				case 'image/pjpeg':
					$CreatedImage = imagecreatefromjpeg($_FILES['ImageFile']['tmp_name']);
					break;
				default:
					die('Unsupported File!'); //output error and exit
			}
		
			$extension = Input::file('ImageFile')->getClientOriginalExtension(); 
			$fileName = md5(time()).'.'.$extension;
			$destinationPath = public_path().'\assets\uploads\gallery';
			$file = Input::file('ImageFile')->move($destinationPath, $fileName);			
			list($CurWidth,$CurHeight)=getimagesize($file);
			
			
			
			$ThumbSquareSize = 650;
			$thumb_DestRandImageName = $destinationPath.'\temp\thumb_'.$fileName;
			$Quality = 90;
			$this->cropImage($CurWidth, $CurHeight, $ThumbSquareSize, $thumb_DestRandImageName,$CreatedImage, $Quality, $ImageType);
			
			$fileName = $fileName.'|||'.$ImageType;
			
			echo '"'.$fileName.'"';
		
	 }
	
	//This function corps image to create exact square images, no matter what its original size!
	  private function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType)
		{	 
			//Check Image size is not 0
			if($CurWidth <= 0 || $CurHeight <= 0) 
			{
				return false;
			}
			
			//abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
			if($CurWidth>$CurHeight)
			{
				$y_offset = 0;
				$x_offset = ($CurWidth - $CurHeight) / 2;
				$square_size 	= $CurWidth - ($x_offset * 2);
			}else{
				$x_offset = 0;
				$y_offset = ($CurHeight - $CurWidth) / 2;
				$square_size = $CurHeight - ($y_offset * 2);
			}
			
			$NewCanves 	= imagecreatetruecolor($iSize, $iSize);	
			if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size))
			{
				switch(strtolower($ImageType))
				{
					case 'image/png':
						imagepng($NewCanves,$DestFolder);
						break;
					case 'image/gif':
						imagegif($NewCanves,$DestFolder);
						break;			
					case 'image/jpeg':
					case 'image/pjpeg':
						imagejpeg($NewCanves,$DestFolder,$Quality);
						break;
					default:
						return false;
				}
			//Destroy image, frees memory	
			if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
			return true;
		
			}
			  
		}
	

}
