<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Image Gallery in Laravel</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />
<link rel="stylesheet" href="{{asset('assets/css/custom.css')}}" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset('assets/css/jquery.Jcrop.min.css')}}" type="text/css" />
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/jcrop_demos.js')}}"></script>
<script language="Javascript">

	// Our simple event handler, called from onChange and onSelect
	// event handlers, as per the Jcrop invocation above
	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
	};
	
	function jcrop_call(){
		$('#custom_preview img').Jcrop({
			aspectRatio: 1,
			onSelect: updateCoords
		});
	}
	
</script>
</head>
<body>
<!-- START PAGE SOURCE -->
<div class="shell">
  <div class="gallery">
    <div class="gallery-t">
      <div class="gallery-holder">
      	@if (Session::has('flash_msg'))   
			<script>
                window.top.location.href = "/"; 
            </script>
		@else
        <div class="gallery-content">
         {{ Form::open(array('files' => true)) }}
        	<h1>Add New Image</h1>
            <p>&nbsp;</p>
            
            <p>
              {{ Form::label('title', 'Title') }}
              <br />
  			  {{Form::text('title', Input::old('title'), array('placeholder' => 'Title'));}}	
            </p>
            
            @if($errors->has('title'))
             <p class="error">{{ $errors->first('title') }}</p>
            @endif
            
            <p>
              {{ Form::label('description', 'Description') }}
              <br />
  			  {{Form::textarea('description', Input::old('description') , array('placeholder' => 'Description'));}}	
            </p>
            @if($errors->has('description'))
             <p class="error">{{ $errors->first('description') }}</p>
            @endif
            
            <p>
              {{ Form::label('tags', 'Tags') }}
              <br />
  			  {{Form::textarea('tags', Input::old('tags') , array('placeholder' => 'Please add tags with comma separated values'));}}	
            </p>
            @if($errors->has('tags'))
             <p class="error">{{ $errors->first('tags') }}</p>
            @endif
            
            
         	<p>
            	{{ Form::label('picture', 'Picture') }}
                <br />
            	{{Form::file('picture', array('onchange' => 'make_preview();'))}}
            </p>            
            @if($errors->has('picture'))
             <p class="error">{{ str_replace(" when path is .", ".",$errors->first('picture')) }}</p>
            @endif
            
            
            	@if(Input::old('path'))
                	<p id="custom_preview" style="display:block">
	                	<img src="/assets/uploads/gallery/temp/thumb_{{Input::old('path')}}" />
                    </p>
                 @else
                 <p id="custom_preview"></p>   
                @endif
            
            <br />
            <p>{{Form::submit('Click here to Upload image', array('id' => 'bigbutton'));}}</p>
            <p>&nbsp;</p>
         	
             {{ Form::hidden('path', Input::old('path'), array('id' => 'path')); }}   
             {{ Form::hidden('image_type', Input::old('image_type'), array('id' => 'image_type')); }}   
             {{ Form::hidden('x', Input::old('x'), array('id' => 'x')); }}   
             {{ Form::hidden('y', Input::old('y'), array('id' => 'y')); }}   
             {{ Form::hidden('w', Input::old('w'), array('id' => 'w')); }}   
             {{ Form::hidden('h', Input::old('h'), array('id' => 'h')); }}   
         
        {{ Form::close() }}
        </div>
        @endif
        
      </div>
    </div>
  </div>
</div>
        
<script>

    
 function make_preview(){  
 	
	$("#picture").hide();
	$("#custom_preview").show();
	$('#custom_preview').html('<img src="/assets/images/ajaxloader.gif" width="128" height="128" >');
	
	var file_data = $("#picture").prop("files")[0];   // Getting the properties of file from file field
	var form_data = new FormData();                  // Creating object of FormData class
	form_data.append("ImageFile", file_data)              // Appending parameter named file with properties of file_field to form_data
	
	   
	   $.ajax({
			url: "/upload_picture",
			dataType: 'script',
			cache: false,
            contentType: false,
			processData: false,
			data : form_data,
			type: "POST",
			success: function(data)
			{
				data = data.replace('"', '');
				data = data.replace('"', '');
				var res = data.split("|||"); 
				$('#path').val(res[0]);
				$('#image_type').val(res[1]);
				$("#custom_preview").html('<img src="/assets/uploads/gallery/temp/thumb_'+res[0]+'" />');
				jcrop_call();
			},
			error: function ()
			{
			}
		});
 }
	 

@if(Input::old('path'))
		jcrop_call();
@endif
   

</script>
<!-- END PAGE SOURCE -->
</body>
</html>