<?php
function humanTiming ($time)
{
		
			$time = time() - $time; // to get the time since that moment
			$tokens = array (
				31536000 => 'year',
				2592000 => 'month',
				604800 => 'week',
				86400 => 'day',
				3600 => 'hour',
				60 => 'minute',
				1 => 'second'
			);
		
			foreach ($tokens as $unit => $text) {
				if ($time < $unit) continue;
				$numberOfUnits = floor($time / $unit);
				return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
			}
		
     }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Image Gallery in Laravel</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset('assets/css/prettyPhoto.css')}}" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset('assets/css/customScroller.css')}}" type="text/css" media="all" />
<script src="{{asset('assets/js/jquery-1.4.2.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/jquery.prettyPhoto.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/jquery.customScroller-1.2.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/js-func.js')}}" type="text/javascript"></script>
<!--[if IE 6]>
<link rel="stylesheet" href="{{asset('assets/css/ie6.css')}}" type="text/css" media="all" />
<script src="{{asset('assets/js/png-fix.js')}}" type="text/javascript"></script>
<![endif]-->
</head>
<body>
<!-- START PAGE SOURCE -->
<div class="shell">
  <div class="gallery">
    <div class="gallery-t">
      <div class="gallery-head">
        <div class="category"><a href="{{URL::to('add-new-image')}}?iframe=true" rel="popupWindow[iframes]" >Add New Image</a></div>
        <div class="search">
          {{ Form::open(array('url' => '/')) }}
            <div class="field">
              {{Form::text('keyword', $keyword, array('placeholder' => 'Search by Title', 'class' => 'blink'));}}
            </div>
            {{Form::submit('', array('class' => 'search-btn notext'));}}
          {{ Form::close() }}
        </div>
        <div class="cl">&nbsp;</div>
      </div>
      <div class="gallery-holder">
        <div class="gallery-content">
         @if(count($images) > 0)
          <ul id="gallery">
          	 @foreach($images as $img) 
             {{-- */$tags_listing = '';/* --}}
             @foreach($img->tags as $tag)
             	{{-- */$tags_listing .= "<a href='/tags/$tag->tag' >$tag->tag</a>, ";/* --}}
             @endforeach
             
             {{-- */$time_ago = strtotime($img->created_at);/* --}}
             {{-- */$time_ago = humanTiming($time_ago);/* --}}
             {{-- */list($CurWidth,$CurHeight)=getimagesize(public_path()."\assets\uploads\gallery\\".$img->path);/* --}}
             {{-- */$image_information = "Size: ".$CurWidth."*".$CurHeight." and uploaded $time_ago ago";/* --}}
             {{-- */$download_link = "<a href='/download/$img->id' style='float:right' >Download Image</a>";/* --}}
             
             {{-- */$tags_listing = rtrim($tags_listing, ', ');/* --}}
                 <li><a href="{{asset('assets/uploads/gallery')}}/{{$img->path}}" rel="prettyPhoto[gallery]" title="{{$img->description}}<br><br> Tags:<br> {{$tags_listing}}<br><br>Image information:<br>{{$image_information}}&nbsp;&nbsp;&nbsp;{{$download_link}}" ><img src="{{asset('assets/uploads/gallery/thumb_')}}{{$img->path}}" alt="{{$img->title}}" title="{{$img->title}}" /></a>
                </li>
            @endforeach
          </ul>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="gallery-b">&nbsp;</div>
</div>
<!-- END PAGE SOURCE -->

<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$("#gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',slideshow:5000, deeplinking: false, autoplay_slideshow: false});
		 $(".category a[rel^='popupWindow[iframes]']").prettyPhoto({
            default_width: 750,
            default_height: '90%',
			deeplinking: false
        });
	});
</script>

</body>
</html>